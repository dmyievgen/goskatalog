import { SEARCH_STRING, IS_NAME_FIRST } from '../env';

export const RESULTS_PATH = './results';
export const RAW_RESULT_PATH = `${RESULTS_PATH}/${SEARCH_STRING}`;
export const RESULT_PATH = `${RAW_RESULT_PATH}${IS_NAME_FIRST ? '_name_first' : ''}`;
export const TIMEOUT_BETWEEN_CALLS = 1500;
export const TIMEOUT_AFTER_FAIL_CALL = 10000;

export const fetchConfigPost = {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
};
