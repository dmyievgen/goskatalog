import { fetchItems, fetchQuantity } from './src/fetches';
import { existResultDirectory } from './src/fileSystemOperations';
import { simpleRecurcedItemDataCollect } from './src/itemOperations';

(async () => {
  await existResultDirectory();
  const itemsQuantity = await fetchQuantity();

  // eslint-disable-next-line no-console
  console.log('items quantity: ', itemsQuantity);

  const itemIds = await fetchItems(0, itemsQuantity);
  await simpleRecurcedItemDataCollect(itemIds, itemsQuantity);
  // eslint-disable-next-line no-console
  console.log('all items processed successfully');
})();
