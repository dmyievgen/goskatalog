import fetch from 'node-fetch';

import { SEARCH_STRING } from '../env';
import {
  generateUrlForItemsFetch,
  generateUrlForQuantityFetch,
  generateUrlForItemFetch,
  generateUrlForImageFetch,
} from './utils';
import { fetchConfigPost } from './constants';

const AbortController = globalThis.AbortController || await import('abort-controller');

export const fetchItems = async (from, limit) => {
  try {
    const response = await fetch(generateUrlForItemsFetch(from, limit), fetchConfigPost);
    const data = await response.json();
    const ids = data?.objects.map(({ id }) => id);
    return ids;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('FAILED REQUEST', SEARCH_STRING, `from: ${from}`, `quantity: ${limit}`);
    throw err;
  }
};

export const fetchItem = async (id) => {
  const controller = new AbortController();
  const timeout = setTimeout(() => {
    // eslint-disable-next-line no-console
    console.log('request aborted');
    controller.abort();
  }, 5000);

  try {
    const response = await fetch(
      generateUrlForItemFetch(id),
      { signal: controller.signal },
    );
    const data = await response.json();
    return { ...data, id };
  } finally {
    clearTimeout(timeout);
  }
};

export const fetchImage = async (imageId, fileName) => {
  const controller = new AbortController();
  const timeout = setTimeout(() => {
    controller.abort();
  }, 5000);

  try {
    const response = await fetch(
      generateUrlForImageFetch(imageId, fileName),
      { signal: controller.signal },
    );
    if (response.status !== 200) throw Error('image not downloaded');
    return response.body;
  } finally {
    clearTimeout(timeout);
  }
};

export const fetchQuantity = async () => {
  try {
    const response = await fetch(generateUrlForQuantityFetch(), fetchConfigPost);
    const data = await response.json();
    return data.statistics[0].count;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log('FAILED REQUEST QUANTITY');
    throw err;
  }
};
