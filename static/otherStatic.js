export const otherStatic = {
  'audit-entity-type': [
    {
      id: 1,
      name: 'пользователь',
      code: 'USER',
    },
    {
      id: 2,
      name: 'музей',
      code: 'MUSEUM',
    },
    {
      id: 3,
      name: 'предмет',
      code: 'EXHIBIT',
    },
    {
      id: 4,
      name: 'заявка',
      code: 'TASK',
    },
    {
      id: 6,
      name: 'автор',
      code: 'AUTHOR',
    },
    {
      id: 7,
      name: 'производитель',
      code: 'PRODUCER',
    },
    {
      id: 8,
      name: 'материалы и техники',
      code: 'TECHNOLOGY',
    },
    {
      id: 9,
      name: 'файл',
      code: 'FILE',
    },
    {
      id: 10,
      name: 'музейная система',
      code: 'MUSEUM_SYSTEM',
    },
    {
      id: 11,
      name: 'внешняя организации',
      code: 'DEAL_ORGANIZATION',
    },
    {
      id: 12,
      name: 'сделка',
      code: 'DEAL',
    },
    {
      id: 13,
      name: 'документ',
      code: 'TOPIC_DOC',
    },
    {
      id: 14,
      name: 'новость',
      code: 'TOPIC_PIECE_OF_NEWS',
    },
    {
      id: 15,
      name: 'faq',
      code: 'TOPIC_FAQ',
    },
    {
      id: 5,
      name: 'ведомственная организация',
      code: 'ORGANIZATION',
    },
    {
      id: 16,
      name: 'страницы',
      code: 'TOPIC_PAGES',
    },
    {
      id: 17,
      name: 'должность',
      code: 'JOB_TITLE',
    },
    {
      id: 18,
      name: 'приказ',
      code: 'ORDER',
    },
    {
      id: 19,
      name: 'книга КП',
      code: 'receipts_book',
    },
  ],
  country: [
    {
      id: 4,
      name: 'Афганистан',
      code: 'AF',
    },
    {
      id: 8,
      name: 'Албания',
      code: 'AL',
    },
    {
      id: 10,
      name: 'Антарктида',
      code: 'AQ',
    },
    {
      id: 12,
      name: 'Алжир',
      code: 'DZ',
    },
    {
      id: 16,
      name: 'Американское Самоа',
      code: 'AS',
    },
    {
      id: 20,
      name: 'Андорра',
      code: 'AD',
    },
    {
      id: 24,
      name: 'Ангола',
      code: 'AO',
    },
    {
      id: 28,
      name: 'Антигуа и Барбуда',
      code: 'AG',
    },
    {
      id: 31,
      name: 'Азербайджан',
      code: 'AZ',
    },
    {
      id: 32,
      name: 'Аргентина',
      code: 'AR',
    },
    {
      id: 36,
      name: 'Австралия',
      code: 'AU',
    },
    {
      id: 40,
      name: 'Австрия',
      code: 'AT',
    },
    {
      id: 44,
      name: 'Багамы',
      code: 'BS',
    },
    {
      id: 48,
      name: 'Бахрейн',
      code: 'BH',
    },
    {
      id: 50,
      name: 'Бангладеш',
      code: 'BD',
    },
    {
      id: 51,
      name: 'Армения',
      code: 'AM',
    },
    {
      id: 52,
      name: 'Барбадос',
      code: 'BB',
    },
    {
      id: 56,
      name: 'Бельгия',
      code: 'BE',
    },
    {
      id: 60,
      name: 'Бермуды',
      code: 'BM',
    },
    {
      id: 64,
      name: 'Бутан',
      code: 'BT',
    },
    {
      id: 68,
      name: 'Боливия, Многонациональное Государство',
      code: 'BO',
    },
    {
      id: 70,
      name: 'Босния и Герцеговина',
      code: 'BA',
    },
    {
      id: 72,
      name: 'Ботсвана',
      code: 'BW',
    },
    {
      id: 74,
      name: 'Остров Буве',
      code: 'BV',
    },
    {
      id: 76,
      name: 'Бразилия',
      code: 'BR',
    },
    {
      id: 84,
      name: 'Белиз',
      code: 'BZ',
    },
    {
      id: 86,
      name: 'Британская Территория В Индийском Океане',
      code: 'IO',
    },
    {
      id: 90,
      name: 'Соломоновы острова',
      code: 'SB',
    },
    {
      id: 92,
      name: 'Виргинские острова, Британские',
      code: 'VG',
    },
    {
      id: 96,
      name: 'Бруней-Даруссалам',
      code: 'BN',
    },
    {
      id: 100,
      name: 'Болгария',
      code: 'BG',
    },
    {
      id: 104,
      name: 'Мьянма',
      code: 'MM',
    },
    {
      id: 108,
      name: 'Бурунди',
      code: 'BI',
    },
    {
      id: 112,
      name: 'Беларусь',
      code: 'BY',
    },
    {
      id: 116,
      name: 'Камбоджа',
      code: 'KH',
    },
    {
      id: 120,
      name: 'Камерун',
      code: 'CM',
    },
    {
      id: 124,
      name: 'Канада',
      code: 'CA',
    },
    {
      id: 132,
      name: 'Кабо-Верде',
      code: 'CV',
    },
    {
      id: 136,
      name: 'Острова Кайман',
      code: 'KY',
    },
    {
      id: 140,
      name: 'Центрально-Африканская Республика',
      code: 'CF',
    },
    {
      id: 144,
      name: 'Шри-Ланка',
      code: 'LK',
    },
    {
      id: 148,
      name: 'Чад',
      code: 'TD',
    },
    {
      id: 152,
      name: 'Чили',
      code: 'CL',
    },
    {
      id: 156,
      name: 'Китай',
      code: 'CN',
    },
    {
      id: 158,
      name: 'Тайвань (китай),',
      code: 'TW',
    },
    {
      id: 162,
      name: 'Остров Рождества',
      code: 'CX',
    },
    {
      id: 166,
      name: 'Кокосовые (Килинг) острова',
      code: 'CC',
    },
    {
      id: 170,
      name: 'Колумбия',
      code: 'CO',
    },
    {
      id: 174,
      name: 'Коморы',
      code: 'KM',
    },
    {
      id: 175,
      name: 'Майотта',
      code: 'YT',
    },
    {
      id: 178,
      name: 'Конго',
      code: 'CG',
    },
    {
      id: 180,
      name: 'Конго, Демократическая Республика',
      code: 'CD',
    },
    {
      id: 184,
      name: 'Острова Кука',
      code: 'CK',
    },
    {
      id: 188,
      name: 'Коста-Рика',
      code: 'CR',
    },
    {
      id: 191,
      name: 'Хорватия',
      code: 'HR',
    },
    {
      id: 192,
      name: 'Куба',
      code: 'CU',
    },
    {
      id: 196,
      name: 'Кипр',
      code: 'CY',
    },
    {
      id: 203,
      name: 'Чехия',
      code: 'CZ',
    },
    {
      id: 204,
      name: 'Бенин',
      code: 'BJ',
    },
    {
      id: 208,
      name: 'Дания',
      code: 'DK',
    },
    {
      id: 212,
      name: 'Доминика',
      code: 'DM',
    },
    {
      id: 214,
      name: 'Доминиканская Республика',
      code: 'DO',
    },
    {
      id: 218,
      name: 'Эквадор',
      code: 'EC',
    },
    {
      id: 222,
      name: 'Эль-Сальвадор',
      code: 'SV',
    },
    {
      id: 226,
      name: 'Экваториальная Гвинея',
      code: 'GQ',
    },
    {
      id: 231,
      name: 'Эфиопия',
      code: 'ET',
    },
    {
      id: 232,
      name: 'Эритрея',
      code: 'ER',
    },
    {
      id: 233,
      name: 'Эстония',
      code: 'EE',
    },
    {
      id: 234,
      name: 'Фарерские острова',
      code: 'FO',
    },
    {
      id: 238,
      name: 'Фолклендские острова (Мальвинские),',
      code: 'FK',
    },
    {
      id: 239,
      name: 'Южная Джорджия и Южные Сандвичевы острова',
      code: 'GS',
    },
    {
      id: 242,
      name: 'Фиджи',
      code: 'FJ',
    },
    {
      id: 246,
      name: 'Финляндия',
      code: 'FI',
    },
    {
      id: 248,
      name: 'Эландские острова',
      code: 'Ах',
    },
    {
      id: 250,
      name: 'Франция',
      code: 'FR',
    },
    {
      id: 254,
      name: 'Французская Гвиана',
      code: 'GF',
    },
    {
      id: 258,
      name: 'Французская Полинезия',
      code: 'PF',
    },
    {
      id: 260,
      name: 'Французские Южные Территории',
      code: 'TF',
    },
    {
      id: 262,
      name: 'Джибути',
      code: 'DJ',
    },
    {
      id: 266,
      name: 'Габон',
      code: 'GA',
    },
    {
      id: 268,
      name: 'Грузия',
      code: 'GE',
    },
    {
      id: 270,
      name: 'Гамбия',
      code: 'GM',
    },
    {
      id: 275,
      name: 'Палестина, Государство',
      code: 'PS',
    },
    {
      id: 276,
      name: 'Германия',
      code: 'DE',
    },
    {
      id: 288,
      name: 'Гана',
      code: 'GH',
    },
    {
      id: 292,
      name: 'Гибралтар',
      code: 'GI',
    },
    {
      id: 296,
      name: 'Кирибати',
      code: 'KI',
    },
    {
      id: 300,
      name: 'Греция',
      code: 'GR',
    },
    {
      id: 304,
      name: 'Гренландия',
      code: 'GL',
    },
    {
      id: 308,
      name: 'Гренада',
      code: 'GD',
    },
    {
      id: 312,
      name: 'Гваделупа',
      code: 'GP',
    },
    {
      id: 316,
      name: 'Гуам',
      code: 'GU',
    },
    {
      id: 320,
      name: 'Гватемала',
      code: 'GT',
    },
    {
      id: 324,
      name: 'Гвинея',
      code: 'GN',
    },
    {
      id: 328,
      name: 'Гайана',
      code: 'GY',
    },
    {
      id: 332,
      name: 'Гаити',
      code: 'HT',
    },
    {
      id: 334,
      name: 'Остров Херд и острова Макдональд',
      code: 'HM',
    },
    {
      id: 336,
      name: 'Папский Престол (государство - город Ватикан),',
      code: 'VA',
    },
    {
      id: 340,
      name: 'Гондурас',
      code: 'HN',
    },
    {
      id: 344,
      name: 'Гонконг',
      code: 'HK',
    },
    {
      id: 348,
      name: 'Венгрия',
      code: 'HU',
    },
    {
      id: 352,
      name: 'Исландия',
      code: 'IS',
    },
    {
      id: 356,
      name: 'Индия',
      code: 'IN',
    },
    {
      id: 360,
      name: 'Индонезия',
      code: 'ID',
    },
    {
      id: 364,
      name: 'Иран (исламская Республика)',
      code: 'IR',
    },
    {
      id: 368,
      name: 'Ирак',
      code: 'IQ',
    },
    {
      id: 372,
      name: 'Ирландия',
      code: 'IE',
    },
    {
      id: 376,
      name: 'Израиль',
      code: 'IL',
    },
    {
      id: 380,
      name: 'Италия',
      code: 'IT',
    },
    {
      id: 384,
      name: "Кот Д'Ивуар",
      code: 'CI',
    },
    {
      id: 388,
      name: 'Ямайка',
      code: 'JM',
    },
    {
      id: 392,
      name: 'Япония',
      code: 'JP',
    },
    {
      id: 398,
      name: 'Казахстан',
      code: 'KZ',
    },
    {
      id: 400,
      name: 'Иордания',
      code: 'JO',
    },
    {
      id: 404,
      name: 'Кения',
      code: 'KE',
    },
    {
      id: 408,
      name: 'Корея, Народно-Демократическая Республика',
      code: 'KP',
    },
    {
      id: 410,
      name: 'Корея, Республика',
      code: 'KR',
    },
    {
      id: 414,
      name: 'Кувейт',
      code: 'KW',
    },
    {
      id: 417,
      name: 'Киргизия',
      code: 'KG',
    },
    {
      id: 418,
      name: 'Лаосская Народно-Демократическая Республика',
      code: 'LA',
    },
    {
      id: 422,
      name: 'Ливан',
      code: 'LB',
    },
    {
      id: 426,
      name: 'Лесото',
      code: 'LS',
    },
    {
      id: 428,
      name: 'Латвия',
      code: 'LV',
    },
    {
      id: 430,
      name: 'Либерия',
      code: 'LR',
    },
    {
      id: 434,
      name: 'Ливия',
      code: 'LY',
    },
    {
      id: 438,
      name: 'Лихтенштейн',
      code: 'LI',
    },
    {
      id: 440,
      name: 'Литва',
      code: 'LT',
    },
    {
      id: 442,
      name: 'Люксембург',
      code: 'LU',
    },
    {
      id: 446,
      name: 'Макао',
      code: 'MO',
    },
    {
      id: 450,
      name: 'Мадагаскар',
      code: 'MG',
    },
    {
      id: 454,
      name: 'Малави',
      code: 'MW',
    },
    {
      id: 458,
      name: 'Малайзия',
      code: 'MY',
    },
    {
      id: 462,
      name: 'Мальдивы',
      code: 'MV',
    },
    {
      id: 466,
      name: 'Мали',
      code: 'ML',
    },
    {
      id: 470,
      name: 'Мальта',
      code: 'MT',
    },
    {
      id: 474,
      name: 'Мартиника',
      code: 'MQ',
    },
    {
      id: 478,
      name: 'Мавритания',
      code: 'MR',
    },
    {
      id: 480,
      name: 'Маврикий',
      code: 'MU',
    },
    {
      id: 484,
      name: 'Мексика',
      code: 'MX',
    },
    {
      id: 492,
      name: 'Монако',
      code: 'MC',
    },
    {
      id: 496,
      name: 'Монголия',
      code: 'MN',
    },
    {
      id: 498,
      name: 'Молдова, Республика',
      code: 'MD',
    },
    {
      id: 499,
      name: 'Черногория',
      code: 'ME',
    },
    {
      id: 500,
      name: 'Монтсеррат',
      code: 'MS',
    },
    {
      id: 504,
      name: 'Марокко',
      code: 'MA',
    },
    {
      id: 508,
      name: 'Мозамбик',
      code: 'MZ',
    },
    {
      id: 512,
      name: 'Оман',
      code: 'OM',
    },
    {
      id: 516,
      name: 'Намибия',
      code: 'NA',
    },
    {
      id: 520,
      name: 'Науру',
      code: 'NR',
    },
    {
      id: 524,
      name: 'Непал',
      code: 'NP',
    },
    {
      id: 528,
      name: 'Нидерланды',
      code: 'NL',
    },
    {
      id: 531,
      name: 'Кюрасао',
      code: 'CW',
    },
    {
      id: 533,
      name: 'Аруба',
      code: 'AW',
    },
    {
      id: 534,
      name: 'Сен-Мартен (нидерландская часть),',
      code: 'SX',
    },
    {
      id: 535,
      name: 'Бонэйр, Синт-Эстатиус и Саба',
      code: 'BQ',
    },
    {
      id: 540,
      name: 'Новая Каледония',
      code: 'NC',
    },
    {
      id: 548,
      name: 'Вануату',
      code: 'VU',
    },
    {
      id: 554,
      name: 'Новая Зеландия',
      code: 'NZ',
    },
    {
      id: 558,
      name: 'Никарагуа',
      code: 'NI',
    },
    {
      id: 562,
      name: 'Нигер',
      code: 'NE',
    },
    {
      id: 566,
      name: 'Нигерия',
      code: 'NG',
    },
    {
      id: 570,
      name: 'Ниуэ',
      code: 'NU',
    },
    {
      id: 574,
      name: 'Остров Норфолк',
      code: 'NF',
    },
    {
      id: 578,
      name: 'Норвегия',
      code: 'NO',
    },
    {
      id: 580,
      name: 'Северные Марианские острова',
      code: 'MP',
    },
    {
      id: 581,
      name: 'Малые Тихоокеанские Отдаленные острова Соединенных Штатов',
      code: 'UM',
    },
    {
      id: 583,
      name: 'Микронезия, Федеративные Штаты',
      code: 'FM',
    },
    {
      id: 584,
      name: 'Маршалловы острова',
      code: 'MH',
    },
    {
      id: 585,
      name: 'Палау',
      code: 'PW',
    },
    {
      id: 586,
      name: 'Пакистан',
      code: 'PK',
    },
    {
      id: 591,
      name: 'Панама',
      code: 'PA',
    },
    {
      id: 598,
      name: 'Папуа-Новая Гвинея',
      code: 'PG',
    },
    {
      id: 600,
      name: 'Парагвай',
      code: 'PY',
    },
    {
      id: 604,
      name: 'Перу',
      code: 'PE',
    },
    {
      id: 608,
      name: 'Филиппины',
      code: 'PH',
    },
    {
      id: 612,
      name: 'Питкерн',
      code: 'PN',
    },
    {
      id: 616,
      name: 'Польша',
      code: 'PL',
    },
    {
      id: 620,
      name: 'Португалия',
      code: 'PT',
    },
    {
      id: 624,
      name: 'Гвинея-Бисау',
      code: 'GW',
    },
    {
      id: 626,
      name: 'Тимор-Лесте',
      code: 'TL',
    },
    {
      id: 630,
      name: 'Пуэрто-Рико',
      code: 'PR',
    },
    {
      id: 634,
      name: 'Катар',
      code: 'QA',
    },
    {
      id: 638,
      name: 'Реюньон',
      code: 'RE',
    },
    {
      id: 642,
      name: 'Румыния',
      code: 'RO',
    },
    {
      id: 643,
      name: 'Россия',
      code: 'RU',
    },
    {
      id: 646,
      name: 'Руанда',
      code: 'RW',
    },
    {
      id: 652,
      name: 'Сен-Бартелеми',
      code: 'BL',
    },
    {
      id: 654,
      name: 'Святая Елена, Остров Вознесения, Тристан-да-кунья',
      code: 'SH',
    },
    {
      id: 659,
      name: 'Сент-Китс и Невис',
      code: 'KN',
    },
    {
      id: 660,
      name: 'Ангилья',
      code: 'AI',
    },
    {
      id: 662,
      name: 'Сент-Люсия',
      code: 'LC',
    },
    {
      id: 663,
      name: 'Сен-Мартен (французская часть),',
      code: 'MF',
    },
    {
      id: 666,
      name: 'Сен-Пьер и Микелон',
      code: 'PM',
    },
    {
      id: 670,
      name: 'Сент-Винсент и Гренадины',
      code: 'VC',
    },
    {
      id: 674,
      name: 'Сан-Марино',
      code: 'SM',
    },
    {
      id: 678,
      name: 'Сан-Томе и Принсипи',
      code: 'ST',
    },
    {
      id: 682,
      name: 'Саудовская Аравия',
      code: 'SA',
    },
    {
      id: 686,
      name: 'Сенегал',
      code: 'SN',
    },
    {
      id: 688,
      name: 'Сербия',
      code: 'RS',
    },
    {
      id: 690,
      name: 'Сейшелы',
      code: 'SC',
    },
    {
      id: 694,
      name: 'Сьерра-Леоне',
      code: 'SL',
    },
    {
      id: 702,
      name: 'Сингапур',
      code: 'SG',
    },
    {
      id: 703,
      name: 'Словакия',
      code: 'SK',
    },
    {
      id: 704,
      name: 'Вьетнам',
      code: 'VN',
    },
    {
      id: 705,
      name: 'Словения',
      code: 'SI',
    },
    {
      id: 706,
      name: 'Сомали',
      code: 'SO',
    },
    {
      id: 710,
      name: 'Южная Африка',
      code: 'ZA',
    },
    {
      id: 716,
      name: 'Зимбабве',
      code: 'ZW',
    },
    {
      id: 724,
      name: 'Испания',
      code: 'ES',
    },
    {
      id: 728,
      name: 'Южный Судан',
      code: 'SS',
    },
    {
      id: 729,
      name: 'Судан',
      code: 'SD',
    },
    {
      id: 732,
      name: 'Западная Сахара',
      code: 'EH',
    },
    {
      id: 740,
      name: 'Суринам',
      code: 'SR',
    },
    {
      id: 744,
      name: 'Шпицберген и Ян Майен',
      code: 'SJ',
    },
    {
      id: 748,
      name: 'Эсватини',
      code: 'SZ',
    },
    {
      id: 752,
      name: 'Швеция',
      code: 'SE',
    },
    {
      id: 756,
      name: 'Швейцария',
      code: 'CH',
    },
    {
      id: 760,
      name: 'Сирийская Арабская Республика',
      code: 'SY',
    },
    {
      id: 762,
      name: 'Таджикистан',
      code: 'TJ',
    },
    {
      id: 764,
      name: 'Таиланд',
      code: 'TH',
    },
    {
      id: 768,
      name: 'Того',
      code: 'TG',
    },
    {
      id: 772,
      name: 'Токелау',
      code: 'TK',
    },
    {
      id: 776,
      name: 'Тонга',
      code: 'TO',
    },
    {
      id: 780,
      name: 'Тринидад и Тобаго',
      code: 'TT',
    },
    {
      id: 784,
      name: 'Объединенные Арабские Эмираты',
      code: 'AE',
    },
    {
      id: 788,
      name: 'Тунис',
      code: 'TN',
    },
    {
      id: 792,
      name: 'Турция',
      code: 'TR',
    },
    {
      id: 795,
      name: 'Туркменистан',
      code: 'TM',
    },
    {
      id: 796,
      name: 'Острова Теркс и Кайкос',
      code: 'TC',
    },
    {
      id: 798,
      name: 'Тувалу',
      code: 'TV',
    },
    {
      id: 800,
      name: 'Уганда',
      code: 'UG',
    },
    {
      id: 804,
      name: 'Украина',
      code: 'UA',
    },
    {
      id: 807,
      name: 'Республика Македония',
      code: 'MK',
    },
    {
      id: 818,
      name: 'Египет',
      code: 'EG',
    },
    {
      id: 826,
      name: 'Соединенное Королевство',
      code: 'GB',
    },
    {
      id: 831,
      name: 'Гернси',
      code: 'GG',
    },
    {
      id: 832,
      name: 'Джерси',
      code: 'JE',
    },
    {
      id: 833,
      name: 'Остров Мэн',
      code: 'IM',
    },
    {
      id: 834,
      name: 'Танзания, Объединенная Республика',
      code: 'TZ',
    },
    {
      id: 840,
      name: 'Соединенные Штаты',
      code: 'US',
    },
    {
      id: 850,
      name: 'Виргинские острова (США),',
      code: 'VI',
    },
    {
      id: 854,
      name: 'Буркина-Фасо',
      code: 'BF',
    },
    {
      id: 858,
      name: 'Уругвай',
      code: 'UY',
    },
    {
      id: 860,
      name: 'Узбекистан',
      code: 'UZ',
    },
    {
      id: 862,
      name: 'Венесуэла (Боливарианская Республика),',
      code: 'VE',
    },
    {
      id: 876,
      name: 'Уоллис и Футуна',
      code: 'WF',
    },
    {
      id: 882,
      name: 'Самоа',
      code: 'WS',
    },
    {
      id: 887,
      name: 'Йемен',
      code: 'YE',
    },
    {
      id: 894,
      name: 'Замбия',
      code: 'ZM',
    },
    {
      id: 895,
      name: 'Абхазия',
      code: 'AB',
    },
    {
      id: 896,
      name: 'Южная Осетия',
      code: 'OS',
    },
  ],
  fund_type: [
    {
      id: 1,
      name: 'государственная',
    },
    {
      id: 2,
      name: 'негосударственная',
    },
  ],
  ownership_type: [
    {
      id: 1,
      name: 'федеральная',
    },
    {
      id: 2,
      name: 'субъекта РФ',
    },
    {
      id: 3,
      name: 'муниципальная',
    },
    {
      id: 4,
      name: 'частная',
    },
  ],
  'exhibit-status': [
    {
      id: 1,
      name: 'Черновик',
      code: 'NEW',
    },
    {
      id: 4,
      name: 'Отправлен на включение в МФ',
      code: 'SENT_FOR_INCLUSION',
    },
    {
      id: 6,
      name: 'Зарегистрирован',
      code: 'REGISTERED',
    },
    {
      id: 7,
      name: 'Отклонен',
      code: 'REJECTED',
    },
    {
      id: 8,
      name: 'Погашен (техническая ошибка)',
      code: 'DELETED',
    },
    {
      id: 10,
      name: 'Отправлен на исключение',
      code: 'SENT_FOR_EXCLUSION',
    },
    {
      id: 11,
      name: 'Исключен из МФ РФ',
      code: 'EXCLUDED',
    },
    {
      id: 12,
      name: 'Мигрирован из ГК 1.0',
      code: 'MIGRATED',
    },
    {
      id: 13,
      name: 'Мигрирован',
      code: 'MIGRATED_GK2',
    },
  ],
  roles: [
    {
      id: 5,
      name: 'Администратор ГИВЦ',
      code: 'admin-devel',
      level: 0,
    },
    {
      id: 1,
      name: 'Администратор музея',
      code: 'admin',
      level: 1,
    },
    {
      id: 10,
      name: 'Работник территориального органа МК РФ в федеральных округах',
      code: 'mincult-federal',
      level: 10,
    },
    {
      id: 4,
      name: 'Работник органа исполнительной власти субъекта Российской Федерации',
      code: 'mincult-regional',
      level: 20,
    },
    {
      id: 11,
      name: 'Работник федерального органа исполнительной власти',
      code: 'federal-executive-agency',
      level: 21,
    },
    {
      id: 3,
      name: 'Работник Минкультуры России',
      code: 'mincult',
      level: 30,
    },
    {
      id: 2,
      name: 'Музейный работник',
      code: 'user',
      level: 40,
    },
    {
      id: 8,
      name: 'Физическое лицо',
      code: 'individual',
      level: 50,
    },
    {
      id: 9,
      name: 'Работник органа местного самоуправления (муниципалитета)',
      code: 'local-government-official',
      level: 50,
    },
    {
      id: 6,
      name: 'Агент АМС',
      code: 'agent-AMS',
      level: 555,
    },
    {
      id: 7,
      name: 'Внешняя информационная система',
      code: 'external-infosystem',
      level: 666,
    },
  ],
  'identifier-type': [
    {
      id: 1,
      name: 'ИНН',
      code: 'INN',
    },
    {
      id: 2,
      name: 'ОГРН',
      code: 'OGRN',
    },
    {
      id: 3,
      name: 'ОКПО',
      code: 'OKPO',
    },
  ],
  'task-type': [
    {
      id: 1,
      name: 'регистрация в ГК',
      code: 'REGISTRATION',
    },
    {
      id: 2,
      name: 'включение в МФ',
      code: 'INCLUSION',
    },
    {
      id: 4,
      name: 'регистрация ЧЛ',
      code: 'REGISTRATION_PRIVATE_MUSEUM',
    },
    {
      id: 5,
      name: 'регистрация НГЧ в ГК',
      code: 'REGISTRATION_PRIVATE_EXHIBIT',
    },
    {
      id: 6,
      name: 'регистрация музея',
      code: 'REGISTRATION_MUSEUM',
    },
    {
      id: 7,
      name: 'корректировка музея',
      code: 'CORRECTION_MUSEUM',
    },
    {
      id: 8,
      name: 'переименование музея',
      code: 'RENAMING_MUSEUM',
    },
    {
      id: 3,
      name: 'исключение из МФ РФ',
      code: 'EXCLUSION',
    },
  ],
  epoch: [
    {
      id: 1,
      name: 'Четвертичный (0 - 2,588 млн. лет назад)',
    },
    {
      id: 2,
      name: 'Неогеновый (2,588 - 23,03 млн. лет назад)',
    },
    {
      id: 3,
      name: 'Палеогеновый (23,03 - 66,0 млн. лет назад)',
    },
    {
      id: 4,
      name: 'Меловой (66,0 - 145,0 млн. лет назад)',
    },
    {
      id: 5,
      name: 'Юрский (145,0 - 201,3 млн. лет назад)',
    },
    {
      id: 6,
      name: 'Триасовый (201,3 - 252,17 млн. лет назад)',
    },
    {
      id: 7,
      name: 'Пермский (252,17 - 298,9 млн. лет назад)',
    },
    {
      id: 8,
      name: 'Каменноугольный (298,9 - 358,9 млн. лет назад)',
    },
    {
      id: 9,
      name: 'Девонский (358,9 - 419,2 млн. лет назад)',
    },
    {
      id: 10,
      name: 'Силурийский (419,2 - 443,4 млн. лет назад)',
    },
    {
      id: 11,
      name: 'Ордовикский (443,4 - 485,4 млн. лет назад)',
    },
    {
      id: 12,
      name: 'Кембрийский (485,4 - 541,0 млн. лет назад)',
    },
  ],
  'deal-subtype': [
    {
      id: 1,
      name: 'Договор на временное использование в целях экспонирования (ВЭ)',
      code: 'EXHIBITION',
      dealTypeId: 11,
    },
    {
      id: 2,
      name: 'Договор на выполнение работ по реставрации (РР)',
      code: 'RESTORATION',
      dealTypeId: 11,
    },
    {
      id: 3,
      name: 'Договор на временное использование музыкальных инструментов, включенных в государственную коллекцию музыкальных инструментов (ВМ)',
      code: 'MUS_SOME_USAGE',
      dealTypeId: 11,
    },
    {
      id: 4,
      name: 'Договор безвозмездного пользования в религиозных целях (БЦ)',
      code: 'REL_GIVING',
      dealTypeId: 11,
    },
    {
      id: 5,
      name: 'До 31.12.2016',
      code: 'BEFORE_16',
      dealTypeId: 6,
    },
    {
      id: 6,
      name: 'После 31.12.2016',
      code: 'AFTER_16',
      dealTypeId: 6,
    },
  ],
  'museum-status': [
    {
      id: 1,
      name: 'Незарегистрированный',
      code: 'UNREGISTERED',
    },
    {
      id: 2,
      name: 'Зарегистрированный',
      code: 'REGISTERED',
    },
    {
      id: 3,
      name: 'Черновик',
      code: 'DRAFT',
    },
    {
      id: 4,
      name: 'Не подлежит регистрации',
      code: 'NOT_FOR_REGISTRATION',
    },
  ],
  'deal-exhibit-status': [
    {
      id: 1,
      name: 'подтвержден',
      code: 'CONFIRMED',
    },
    {
      id: 2,
      name: 'возвращен',
      code: 'RETURNED',
    },
  ],
  'attr-type': [
    {
      id: 1,
      name: 'ID музейной системы',
      code: 'MUSSYSTEMID',
    },
    {
      id: 2,
      name: 'Место производства',
      code: 'PRODUCTIONPLACE',
    },
    {
      id: 3,
      name: 'Производитель',
      code: 'PRODUCTIONORG',
    },
    {
      id: 4,
      name: 'Наименование',
      code: 'NAME',
    },
    {
      id: 5,
      name: 'Время создания',
      code: 'CREATIONTIME',
    },
    {
      id: 6,
      name: 'Период создания с',
      code: 'CREATIONSTART',
    },
    {
      id: 7,
      name: 'Период создания по',
      code: 'CREATIONEND',
    },
    {
      id: 8,
      name: 'Количество составляющих',
      code: 'PARTSCOUNT',
    },
    {
      id: 9,
      name: 'Номер по ГИК',
      code: 'KPNUMBER',
    },
    {
      id: 10,
      name: 'Вес',
      code: 'WEIGHT',
    },
    {
      id: 11,
      name: 'Инвентарный номер',
      code: 'INVNUMBER',
    },
    {
      id: 12,
      name: 'Матералы',
      code: 'MATERIALANDTECHNIQUE',
    },
    {
      id: 13,
      name: 'Размеры',
      code: 'DIMENSIONS',
    },
    {
      id: 14,
      name: 'Описание',
      code: 'SHORTDESCRIPTION',
    },
    {
      id: 15,
      name: 'Номер акта приема на постоянное хранение',
      code: 'ACTNUMBERANDDATE',
    },
    {
      id: 16,
      name: 'Бюджет',
      code: 'IDBUDGET',
    },
    {
      id: 17,
      name: 'Типология ГК',
      code: 'TYPOLOGYGK',
    },
    {
      id: 18,
      name: 'Номер в ГК',
      code: 'GKNUMBER',
    },
    {
      id: 19,
      name: 'FZK',
      code: 'FZK',
    },
    {
      id: 20,
      name: 'Признак коллекции',
      code: 'COLLECTION',
    },
    {
      id: 21,
      name: 'Идентификатор коллекции',
      code: 'COLLECTIONID',
    },
    {
      id: 22,
      name: 'Типология',
      code: 'TYPOLOGY',
    },
    {
      id: 23,
      name: 'Способ поступления',
      code: 'ENTRANCEWAY',
    },
    {
      id: 24,
      name: 'Авторы',
      code: 'AUTHORS',
    },
    {
      id: 25,
      name: 'Источник поступления',
      code: 'ENTRANCESOURCE',
    },
    {
      id: 26,
      name: 'Корректировка акта',
      code: 'ACTNUMCORR',
    },
    {
      id: 27,
      name: 'Дата корректировки акта',
      code: 'ACTNUMCORRDATE',
    },
    {
      id: 28,
      name: 'Дата фиксации размеров',
      code: 'DIMCORRTDATE',
    },
    {
      id: 29,
      name: 'Корректировка размера',
      code: 'DIMCORRT',
    },
    {
      id: 30,
      name: 'Дата фиксации веса',
      code: 'WEIGHTCORRDATE',
    },
    {
      id: 31,
      name: 'Требуется корректировка веса',
      code: 'WEIGHTCORR',
    },
    {
      id: 32,
      name: 'Дата ограничения на публикацию',
      code: 'PUBLIMDATE',
    },
    {
      id: 33,
      name: 'Вывоз',
      code: 'TRANSPORTATION',
    },
    {
      id: 34,
      name: 'Суммарный номер',
      code: 'SUMNUMBER',
    },
    {
      id: 35,
      name: 'Номер спецучета',
      code: 'SPECNUMBER',
    },
    {
      id: 36,
      name: 'MFOrderID',
      code: 'MFORDERID',
    },
    {
      id: 37,
      name: 'MessageID',
      code: 'MESSAGEID',
    },
    {
      id: 38,
      name: 'SystemID',
      code: 'SYSTEMID',
    },
    {
      id: 39,
      name: 'Тип собственности',
      code: 'OWNERSHIPTYPE',
    },
    {
      id: 40,
      name: 'Тип управления',
      code: 'MANAGEMENT_ORDER',
    },
    {
      id: 41,
      name: 'Часть музейного фонда',
      code: 'FUND',
    },
    {
      id: 42,
      name: 'Статус миграции',
      code: 'MIGR_STATUS',
    },
    {
      id: 43,
      name: 'CIS_MUS_OBJECT_ID',
      code: 'MUS_OBJECT_ID',
    },
    {
      id: 44,
      name: 'MUS_SYS_COLLECTION',
      code: 'MUS_SYS_COLLECTION',
    },
    {
      id: 45,
      name: 'REG_NUM_COLLECTION',
      code: 'REG_NUM_COLLECTION',
    },
    {
      id: 46,
      name: 'Идентификатор музея в CIS',
      code: 'CIS_MUSEUM_ID',
    },
    {
      id: 47,
      name: 'AUTHORITY_TYPE',
      code: 'AUTHORITY_TYPE',
    },
    {
      id: 48,
      name: 'ORG_FORM',
      code: 'ORG_FORM',
    },
    {
      id: 49,
      name: 'SYSTEMS',
      code: 'SYSTEMS',
    },
    {
      id: 50,
      name: 'STATUS',
      code: 'STATUS',
    },
    {
      id: 51,
      name: 'FAX',
      code: 'FAX',
    },
    {
      id: 52,
      name: 'PHONES',
      code: 'PHONES',
    },
    {
      id: 53,
      name: 'CIS_MUSEUM_ID',
      code: 'MUSEUM_ID',
    },
    {
      id: 54,
      name: 'KOPUK',
      code: 'KOPUK',
    },
  ],
  'federal-area': [
    {
      id: 30,
      name: 'Центральный федеральный округ',
    },
    {
      id: 31,
      name: 'Северо-Западный федеральный округ',
    },
    {
      id: 33,
      name: 'Приволжский федеральный округ',
    },
    {
      id: 34,
      name: 'Уральский федеральный округ',
    },
    {
      id: 35,
      name: 'Сибирский федеральный округ',
    },
    {
      id: 36,
      name: 'Дальневосточный федеральный округ',
    },
    {
      id: 37,
      name: 'Южный федеральный округ',
    },
    {
      id: 38,
      name: 'Северо-Кавказский федеральный округ',
    },
  ],
  'import-status': [
    {
      id: 1,
      name: 'Новый',
      code: 'NEW',
    },
    {
      id: 3,
      name: 'Выгружен',
      code: 'DONE',
    },
    {
      id: 4,
      name: 'Ошибка',
      code: 'ERROR',
    },
    {
      id: 5,
      name: 'В очереди',
      code: 'QUEUE',
    },
    {
      id: 6,
      name: 'Загружается',
      code: 'IN_PROCESS',
    },
    {
      id: 7,
      name: 'Загружен',
      code: 'LOADED',
    },
    {
      id: 8,
      name: 'Временая ошибка',
      code: 'TEMP_ERROR',
    },
  ],
  'deal-side-type': [
    {
      id: 1,
      name: 'Музей',
      code: 'MUSEUM',
    },
    {
      id: 2,
      name: 'Министерство Культуры РФ',
      code: 'MKRF',
    },
    {
      id: 3,
      name: 'Организация',
      code: 'ORGANIZATION',
    },
  ],
  currency: [
    {
      id: 1,
      name: 'Российский рубль',
      code: 'RUB',
      currencySymbol: '₽',
    },
    {
      id: 2,
      name: 'Евро',
      code: 'EUR',
      currencySymbol: '€',
    },
    {
      id: 3,
      name: 'Фунт стерлингов',
      code: 'GBP',
      currencySymbol: '₤',
    },
    {
      id: 4,
      name: 'Доллар США',
      code: 'USD',
      currencySymbol: '$',
    },
    {
      id: 5,
      name: 'Иена',
      code: 'JPY',
      currencySymbol: '¥',
    },
  ],
  'museum-subclass': [
    {
      id: 1,
      name: 'да',
      code: 'museum',
      classId: 1,
    },
    {
      id: 2,
      name: 'нет',
      code: 'organization',
      classId: 1,
    },
    {
      id: 3,
      name: 'отсутствует',
      code: 'none',
      classId: 1,
    },
    {
      id: 4,
      name: 'Минкультуры',
      code: 'minkult',
      classId: 3,
    },
  ],
  'management-order': [
    {
      id: 1,
      name: 'собственность',
    },
    {
      id: 2,
      name: 'оперативное управление',
    },
    {
      id: 3,
      name: 'временное пользование',
    },
  ],
  'topic-type': [
    {
      id: 1,
      name: 'документы',
      code: 'DOCS',
    },
    {
      id: 2,
      name: 'новости',
      code: 'NEWS',
    },
    {
      id: 3,
      name: 'часто задаваемые вопросы',
      code: 'FAQ',
    },
    {
      id: 4,
      name: 'страницы',
      code: 'PAGES',
    },
  ],
  'legal-form': [
    {
      id: 1,
      name: 'Государственное учреждение культуры',
    },
    {
      id: 2,
      name: 'Федеральное государственное учреждение культуры',
    },
    {
      id: 3,
      name: 'Муниципальное бюджетное учреждение культуры',
    },
  ],
  budget_type: [
    {
      id: 1,
      name: 'федеральный',
    },
    {
      id: 2,
      name: 'субъекта РФ',
    },
    {
      id: 3,
      name: 'муниципальный',
    },
    {
      id: 4,
      name: 'собственный (внебюджетные средства)',
    },
  ],
  'exclusion-cause': [
    {
      id: 1,
      name: 'Утрата',
      code: 'LOSS',
      obsolete: false,
    },
    {
      id: 2,
      name: 'Разрушение',
      code: 'DESTRUCTION',
      obsolete: false,
    },
    {
      id: 3,
      name: 'Ошибочная экспертиза',
      code: 'ERROR_EXPERTISE',
      obsolete: false,
    },
    {
      id: 4,
      name: 'Судебное решение',
      code: 'JUDICIAL_DECISION',
      obsolete: false,
    },
    {
      id: 5,
      name: 'Постановление Правительства',
      code: 'GOVERNMENT_RESOLUTION',
      obsolete: true,
    },
    {
      id: 6,
      name: 'Федеральный закон',
      code: 'FEDERAL_LAW',
      obsolete: true,
    },
    {
      id: 7,
      name: 'Дважды записанный номер',
      code: 'DUPLICATE_NUMBER',
      obsolete: true,
    },
    {
      id: 8,
      name: 'Обмен',
      code: 'EXCHANGE',
      obsolete: false,
    },
    {
      id: 9,
      name: 'Иное',
      code: 'OTHER',
      obsolete: false,
    },
  ],
  'museum-system': [
    {
      id: 1,
      name: 'КАМИС 2000',
      code: null,
    },
    {
      id: 3,
      name: 'АС МУЗЕЙ 2',
      code: null,
    },
    {
      id: 4,
      name: 'АС МУЗЕЙ 3',
      code: null,
    },
    {
      id: 5,
      name: 'АС МУЗЕЙ 4',
      code: null,
    },
    {
      id: 7,
      name: 'АС УМЦ',
      code: null,
    },
    {
      id: 8,
      name: 'АСУ МЦ',
      code: null,
    },
    {
      id: 9,
      name: 'КАМИС 5.0',
      code: null,
    },
    {
      id: 11,
      name: 'ИС МУЗЕЙ 2013',
      code: null,
    },
    {
      id: 12,
      name: 'HIDA4',
      code: null,
    },
    {
      id: 13,
      name: 'МИС',
      code: null,
    },
    {
      id: 14,
      name: 'отсутствует',
      code: null,
    },
    {
      id: 15,
      name: 'АИС ФОНД',
      code: null,
    },
    {
      id: 16,
      name: 'ИС "Музеи"',
      code: null,
    },
    {
      id: 17,
      name: 'Ника-Музей 2.5.',
      code: null,
    },
    {
      id: 18,
      name: '"Специальная информационная система REM"',
      code: null,
    },
    {
      id: 19,
      name: 'Fox.Pro',
      code: null,
    },
    {
      id: 20,
      name: 'Microsoft Access 2003',
      code: null,
    },
    {
      id: 21,
      name: 'ПАРУС',
      code: null,
    },
    {
      id: 22,
      name: 'СУБД MS Access ',
      code: null,
    },
    {
      id: 23,
      name: 'АИС',
      code: null,
    },
    {
      id: 24,
      name: '1С:Музей',
      code: null,
    },
    {
      id: 41,
      name: 'Эйдотека РОСФОТО',
      code: null,
    },
    {
      id: 42,
      name: 'FMCOL',
      code: null,
    },
    {
      id: 43,
      name: 'КАМИС-Онлайн',
      code: null,
    },
    {
      id: 44,
      name: 'КАМИС 5',
      code: null,
    },
    {
      id: 45,
      name: 'АИС Музей',
      code: null,
    },
    {
      id: 46,
      name: 'АИС "Медное"',
      code: null,
    },
    {
      id: 47,
      name: 'АИС "Катынь"',
      code: null,
    },
    {
      id: 48,
      name: 'PSD Home Museum v1.0.',
      code: null,
    },
    {
      id: 49,
      name: 'истори',
      code: null,
    },
    {
      id: 51,
      name: 'КАМИС 3.0',
      code: null,
    },
    {
      id: 52,
      name: 'КАМИС 5.2',
      code: null,
    },
    {
      id: 53,
      name: 'КАМИС 2.3',
      code: null,
    },
    {
      id: 54,
      name: 'Программа "Хранение"',
      code: null,
    },
    {
      id: 55,
      name: 'АС "Музей" ГИВЦ',
      code: null,
    },
    {
      id: 56,
      name: 'БДНТМЗ',
      code: null,
    },
  ],
  'employee-role': [
    {
      id: 1,
      name: 'руководитель',
      code: 'DIRECTOR',
    },
    {
      id: 2,
      name: 'хранитель',
      code: 'LEAD_SAVER',
    },
    {
      id: 3,
      name: 'подписант',
      code: 'SIGNED',
    },
  ],
  'weight-unit': [
    {
      id: 1,
      name: 'граммы',
    },
    {
      id: 2,
      name: 'килограммы',
    },
    {
      id: 3,
      name: 'тонны',
    },
  ],
  'contact-subtype': [
    {
      id: 1,
      name: 'Справочный',
      code: 'INFO',
      contactTypeId: 1,
    },
    {
      id: 2,
      name: 'Главный хранитель музея',
      code: 'LEAD_SAVER',
      contactTypeId: 1,
    },
    {
      id: 3,
      name: 'Отдел хранения музея',
      code: 'DEPARTMENT_LEADER',
      contactTypeId: 1,
    },
    {
      id: 4,
      name: 'Директор музея',
      code: 'DIRECTOR',
      contactTypeId: 1,
    },
    {
      id: 5,
      name: 'Факс',
      code: 'FAX',
      contactTypeId: 1,
    },
    {
      id: 6,
      name: 'Юридический',
      code: 'BUSINESS',
      contactTypeId: 4,
    },
    {
      id: 7,
      name: 'Фактический',
      code: 'PHYSICAL',
      contactTypeId: 4,
    },
  ],
  'acquire-method': [
    {
      id: 1,
      name: 'Дар',
      code: null,
      obsolete: false,
    },
    {
      id: 3,
      name: 'Закупка',
      code: null,
      obsolete: false,
    },
    {
      id: 4,
      name: 'Из старых поступлений',
      code: null,
      obsolete: true,
    },
    {
      id: 5,
      name: 'Довоенное собрание музея',
      code: null,
      obsolete: true,
    },
    {
      id: 6,
      name: 'Обмен',
      code: null,
      obsolete: true,
    },
    {
      id: 7,
      name: 'Экспедиция',
      code: null,
      obsolete: true,
    },
    {
      id: 8,
      name: 'Переведено из ВХ',
      code: null,
      obsolete: true,
    },
    {
      id: 9,
      name: 'Сбор',
      code: null,
      obsolete: true,
    },
    {
      id: 10,
      name: 'Завещание',
      code: null,
      obsolete: true,
    },
    {
      id: 11,
      name: 'Неизвестен',
      code: null,
      obsolete: true,
    },
    {
      id: 12,
      name: 'Находка',
      code: null,
      obsolete: true,
    },
    {
      id: 13,
      name: 'Конфискация',
      code: null,
      obsolete: true,
    },
    {
      id: 14,
      name: 'Реституция',
      code: null,
      obsolete: true,
    },
    {
      id: 15,
      name: 'Раскопки',
      code: null,
      obsolete: true,
    },
    {
      id: 16,
      name: 'Прочее',
      code: null,
      obsolete: true,
    },
    {
      id: 17,
      name: 'Национализация',
      code: null,
      obsolete: true,
    },
    {
      id: 2,
      name: 'Безвозмездная передача',
      code: null,
      obsolete: false,
    },
    {
      id: 18,
      name: 'Пожертвование',
      code: null,
      obsolete: false,
    },
  ],
  'task-status': [
    {
      id: 2,
      name: 'обрабатывается',
      code: 'IN_PROGRESS',
    },
    {
      id: 3,
      name: 'обработана',
      code: 'COMPLETED',
    },
    {
      id: 5,
      name: 'экспортирована',
      code: 'EXPORTED',
    },
    {
      id: 4,
      name: 'отклонена',
      code: 'REFUSED',
    },
    {
      id: 6,
      name: 'на доработке',
      code: 'RETURNED',
    },
    {
      id: 1,
      name: 'черновик',
      code: 'NEW',
    },
  ],
  'contact-type': [
    {
      id: 1,
      name: 'телефон',
      code: 'PHONE',
    },
    {
      id: 2,
      name: 'адрес электронной почты',
      code: 'EMAIL',
    },
    {
      id: 3,
      name: 'адрес сайта',
      code: 'WEB',
    },
    {
      id: 4,
      name: 'адрес здания',
      code: 'BUILDING_ADDRESS',
    },
  ],
  'exhibit-category': [
    {
      id: 1,
      name: '1',
      code: '1',
    },
    {
      id: 2,
      name: '2',
      code: '2',
    },
    {
      id: 3,
      name: '3',
      code: '3',
    },
  ],
  subordination_type: [
    {
      id: 1,
      name: 'филиал или обособленное структурное подразделение',
      code: 'BRANCH',
    },
    {
      id: 2,
      name: 'юридическое лицо',
      code: 'HEAD',
    },
  ],
  'import-action': [
    {
      id: 1,
      name: 'Регистрация в ГК',
      code: 'Registration',
    },
    {
      id: 2,
      name: 'Включение в ГК',
      code: 'Inclusion',
    },
    {
      id: 3,
      name: 'Коррекция',
      code: 'Correction',
    },
    {
      id: 4,
      name: 'Исключение из ГК',
      code: 'Exclusion',
    },
    {
      id: 5,
      name: 'Экспорт',
      code: 'Export',
    },
  ],
  typology: [
    {
      id: 1,
      name: 'живопись',
      code: null,
      obsolete: false,
    },
    {
      id: 2,
      name: 'графика',
      code: null,
      obsolete: false,
    },
    {
      id: 3,
      name: 'скульптура',
      code: null,
      obsolete: false,
    },
    {
      id: 5,
      name: 'предметы нумизматики',
      code: null,
      obsolete: false,
    },
    {
      id: 6,
      name: 'предметы археологии',
      code: null,
      obsolete: false,
    },
    {
      id: 8,
      name: 'оружие',
      code: null,
      obsolete: false,
    },
    {
      id: 10,
      name: 'предметы естественнонаучной коллекции',
      code: null,
      obsolete: false,
    },
    {
      id: 12,
      name: 'прочие',
      code: null,
      obsolete: false,
    },
    {
      id: 13,
      name: 'предметы прикладного искусства, быта и этнографии',
      code: null,
      obsolete: false,
    },
    {
      id: 14,
      name: 'редкие книги',
      code: null,
      obsolete: false,
    },
    {
      id: 15,
      name: 'документы',
      code: null,
      obsolete: false,
    },
    {
      id: 16,
      name: 'предметы печатной продукции',
      code: null,
      obsolete: false,
    },
    {
      id: 18,
      name: 'предметы минералогической коллекции',
      code: null,
      obsolete: false,
    },
    {
      id: 4,
      name: 'изделия прикладного искусства',
      code: null,
      obsolete: true,
    },
    {
      id: 7,
      name: 'предметы этнографии',
      code: null,
      obsolete: true,
    },
    {
      id: 9,
      name: 'документы, редкие книги',
      code: null,
      obsolete: true,
    },
    {
      id: 11,
      name: 'предметы техники',
      code: null,
      obsolete: false,
    },
    {
      id: 17,
      name: 'фотографии и негативы',
      code: null,
      obsolete: false,
    },
  ],
  'deal-type': [
    {
      id: 11,
      name: 'временное использование предмета',
      code: 'TEMPORARY_USAGE',
      parentId: null,
    },
    {
      id: 6,
      name: 'безвозмездное пользование',
      code: 'PERMANENT_USAGE',
      parentId: null,
    },
    {
      id: 3,
      name: 'дарение',
      code: 'GIVING',
      parentId: null,
    },
    {
      id: 8,
      name: 'доверительное управление',
      code: 'TRUST_ADM_PROPERTY',
      parentId: null,
    },
    {
      id: 4,
      name: 'купля-продажа',
      code: 'PURCHASE',
      parentId: null,
    },
    {
      id: 1,
      name: 'мена',
      code: 'EXCHANGE',
      parentId: null,
    },
    {
      id: 9,
      name: 'наследование по закону/завещанию',
      code: 'HEREDITARY_SUCCESSION',
      parentId: null,
    },
    {
      id: 2,
      name: 'пожертвование',
      code: 'DONATION',
      parentId: null,
    },
    {
      id: 12,
      name: 'продление временного использования предметов',
      code: 'TEMPORARY_USAGE_EXTENSION',
      parentId: 11,
    },
    {
      id: 13,
      name: 'частичный возврат предметов во временном пользовании',
      code: 'TEMPORARY_USAGE_PARTIAL_RETURN',
      parentId: 11,
    },
    {
      id: 14,
      name: 'добавление юридического лица для сделок временного пользования',
      code: 'TEMPORARY_USAGE_ORG_ADDING',
      parentId: 11,
    },
  ],
  'deal-status': [
    {
      id: 1,
      name: 'черновик',
      code: 'DRAFT',
    },
    {
      id: 2,
      name: 'отправлена на подписание Вышестоящей организации',
      code: 'SENT_FOR_REGISTRATION',
    },
    {
      id: 3,
      name: 'зарегистрирована',
      code: 'REGISTERED',
    },
    {
      id: 4,
      name: 'отклонена',
      code: 'REJECTED',
    },
    {
      id: 5,
      name: 'отправлена на регистрацию и подписание в Минкультуры России',
      code: 'SENT_FOR_MINCULT_REGISTRATION',
    },
    {
      id: 6,
      name: 'отправлена на закрытие',
      code: 'SENT_FOR_CLOSING',
    },
    {
      id: 7,
      name: 'закрыта',
      code: 'CLOSED',
    },
    {
      id: 8,
      name: 'в архиве',
      code: 'ARCHIVED',
    },
    {
      id: 9,
      name: 'отправлена на cогласование с МКРФ',
      code: 'SENT_FOR_MINCULT_AGREEMENT',
    },
    {
      id: 10,
      name: ' согласование с Вышестоящей организацией',
      code: 'SENT_FOR_UMBRELLA_AGREEMENT',
    },
    {
      id: 11,
      name: 'подписание музеем',
      code: 'MUSEUM_SIGNED',
    },
  ],
  'audit-event-type': [
    {
      id: 1,
      name: 'создание',
      code: 'CREATE',
    },
    {
      id: 3,
      name: 'изменение',
      code: 'UPDATE',
    },
    {
      id: 4,
      name: 'удаление',
      code: 'DELETE',
    },
    {
      id: 5,
      name: 'импорт',
      code: 'IMPORT',
    },
    {
      id: 6,
      name: 'экспорт',
      code: 'EXPORT',
    },
    {
      id: 7,
      name: 'экспорт во внешний ГК',
      code: 'EXPORT_GK',
    },
  ],
  'museum-type': [
    {
      id: 4,
      name: 'Краеведческий',
      code: '4',
      group: 'Общего профиля',
    },
    {
      id: 7,
      name: 'Музей-заповедник',
      code: '7',
      group: 'Общего профиля',
    },
    {
      id: 1,
      name: 'Естественно-научный',
      code: '1',
      group: 'Естественнонаучный',
    },
    {
      id: 14,
      name: 'Биологический, Зоологический, Ботанический',
      code: '14',
      group: 'Естественнонаучный',
    },
    {
      id: 15,
      name: 'Геологический, Минералогический, Палеонтологический, Почвенный',
      code: '15',
      group: 'Естественнонаучный',
    },
    {
      id: 9,
      name: 'Природно-ландшафтный',
      code: '9',
      group: 'Естественнонаучный',
    },
    {
      id: 3,
      name: 'Исторический',
      code: '3',
      group: 'Исторический',
    },
    {
      id: 16,
      name: 'Археологический',
      code: '16',
      group: 'Исторический',
    },
    {
      id: 13,
      name: 'Этнографический, Религии',
      code: '13',
      group: 'Исторический',
    },
    {
      id: 17,
      name: 'Историко-бытовой',
      code: '17',
      group: 'Исторический',
    },
    {
      id: 18,
      name: 'Военно-исторический',
      code: '18',
      group: 'Исторический',
    },
    {
      id: 19,
      name: 'Антропологический, Медицинский, Спортивный',
      code: '19',
      group: 'Исторический',
    },
    {
      id: 12,
      name: 'Художественный',
      code: '12',
      group: 'Музей искусств',
    },
    {
      id: 20,
      name: 'Декоративно-прикладного и народного искусства',
      code: '20',
      group: 'Музей искусств',
    },
    {
      id: 21,
      name: 'Музей кино, фотографии',
      code: '21',
      group: 'Музей искусств',
    },
    {
      id: 22,
      name: 'Архитектурный',
      code: '22',
      group: 'Музей искусств',
    },
    {
      id: 5,
      name: 'Литературный',
      code: '5',
      group: 'Музей искусств',
    },
    {
      id: 8,
      name: 'Театральный, Музыкальный, музей цирка',
      code: '8',
      group: 'Музей искусств',
    },
    {
      id: 11,
      name: 'Технический',
      code: '11',
      group: 'Научно-технический',
    },
    {
      id: 23,
      name: 'Музей науки',
      code: '23',
      group: 'Научно-технический',
    },
    {
      id: 24,
      name: 'Музей оружия',
      code: '24',
      group: 'Научно-технический',
    },
    {
      id: 25,
      name: 'Информационные технологии и связь',
      code: '25',
      group: 'Научно-технический',
    },
    {
      id: 26,
      name: 'Энергетика, транспорт',
      code: '26',
      group: 'Научно-технический',
    },
    {
      id: 27,
      name: 'Машиностроение, Приборостроение, Металлообработка, Металлургия',
      code: '27',
      group: 'Научно-технический',
    },
    {
      id: 28,
      name: 'Строительство',
      code: '28',
      group: 'Научно-технический',
    },
    {
      id: 29,
      name: 'Химическая, пищевая и обрабатывающая промышленность',
      code: '29',
      group: 'Научно-технический',
    },
    {
      id: 30,
      name: 'Прочие отрасли промышленности',
      code: '30',
      group: 'Научно-технический',
    },
  ],
  'museum-class': [
    {
      id: 1,
      name: 'организация',
      code: 'GOVERNMENT',
    },
    {
      id: 2,
      name: 'физическое лицо',
      code: 'PRIVATE',
    },
    {
      id: 3,
      name: 'вышестоящая организация',
      code: 'ORGANIZATION',
    },
  ],
  'dim-unit': [
    {
      id: 1,
      name: 'миллиметры',
    },
    {
      id: 2,
      name: 'сантиметры',
    },
    {
      id: 3,
      name: 'метры',
    },
  ],
  'deal-reason': [
    {
      id: 1,
      name: 'подтверждена регистрация',
      code: 'CONFIRM_REGISTRATION',
    },
    {
      id: 2,
      name: 'зарегистрирована',
      code: 'REGISTER',
    },
    {
      id: 3,
      name: 'переработана',
      code: 'REWORK',
    },
    {
      id: 4,
      name: 'отклонена',
      code: 'REJECT',
    },
    {
      id: 5,
      name: 'подтверждено закрытие',
      code: 'CONFIRM_CLOSING',
    },
    {
      id: 6,
      name: 'закрыта',
      code: 'CLOSE',
    },
    {
      id: 7,
      name: 'откат по причине смены подписанта',
      code: 'CHANGE_SIGNER',
    },
  ],
};
