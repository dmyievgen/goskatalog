import { fetchItem } from './fetches';
import { collectRejectedIdsFromResult, setTimeoutPromised, ItemCounter } from './utils';
import { saveImages, createItemDescriptionFile } from './fileSystemOperations';
import { TIMEOUT_BETWEEN_CALLS, TIMEOUT_AFTER_FAIL_CALL } from './constants';

const processItem = async (id, quantity, counter) => {
  try {
    const item = await fetchItem(id);
    await saveImages(item);
    await createItemDescriptionFile(item);
    // eslint-disable-next-line no-console
    console.log(`${((counter.items / quantity) * 100).toFixed()}%`, item.id);
    counter.addItem();
    return id;
  } catch (err) {
    // eslint-disable-next-line no-console
    console.log(`failed item ${id}`);
    throw id;
  }
};

// removed because of bad work with huge items quantity
export const recurcedItemDataCollect = async (itemIds, quantity) => {
  const itemCounter = new ItemCounter();
  const runItemCollection = async (items) => {
    const itemsProcessingPromises = items.map((itemId, key) => {
      const timeToWaitBeforeExecution = key * TIMEOUT_BETWEEN_CALLS;
      return setTimeoutPromised(
        () => processItem(itemId, quantity, itemCounter),
        timeToWaitBeforeExecution,
      );
    });

    const promisesResult = await Promise.allSettled(itemsProcessingPromises);
    const rejectedItems = collectRejectedIdsFromResult(promisesResult);

    if (rejectedItems.length) {
      // eslint-disable-next-line no-console
      console.log(`${rejectedItems.length} items are not downloaded. After few seconds next iteration of download will be started`);
      await setTimeoutPromised(() => {}, TIMEOUT_AFTER_FAIL_CALL);
      await runItemCollection(rejectedItems);
    }
  };

  await runItemCollection(itemIds);
};

export const simpleRecurcedItemDataCollect = async (ids, quantity) => {
  const itemCounter = new ItemCounter();
  const processItems = async (itemIds) => {
    const failedItems = [];
    for (let i = 0; i < itemIds.length; i++) {
      try {
        await processItem(itemIds[i], quantity, itemCounter);
      } catch {
        try {
          // eslint-disable-next-line no-console
          console.log(`item ${itemIds[i]} failed.  waiting for repeat`);
          await setTimeoutPromised(() => {}, TIMEOUT_AFTER_FAIL_CALL);
          await processItem(itemIds[i], quantity, itemCounter);
        } catch {
          failedItems.push(itemIds[i]);
          // eslint-disable-next-line no-console
          console.log(`item ${itemIds[i]} failed again.  item added to failed list`);
        }
      }
    }

    if (failedItems.length) {
      // eslint-disable-next-line no-console
      console.log(`${failedItems.length} items are not downloaded. After few seconds next iteration of download will be started`);
      await setTimeoutPromised(() => {}, TIMEOUT_AFTER_FAIL_CALL);
      await processItems(failedItems);
    }
  };

  await processItems(ids);
};
