import fs from 'fs';
import fsPromises from 'fs/promises';

import { fetchImage } from './fetches';
import { RESULT_PATH } from './constants';
import { prepareItemDescriptionText, prepareFileName } from './utils';

export const existResultDirectory = async () => {
  try {
    await fsPromises.rm(RESULT_PATH, { recursive: true });
    // eslint-disable-next-line no-empty
  } catch {}
  await fsPromises.mkdir(RESULT_PATH, { recursive: true });
};

export const createItemDescriptionFile = async (item) => {
  const { id, name } = item;
  const fileName = `${prepareFileName(id, name)}.txt`;
  const descriptionText = prepareItemDescriptionText(item);

  await fsPromises.writeFile(`${RESULT_PATH}/${fileName}`, descriptionText);
};

export const saveImages = async (item) => {
  const { id: itemId, name: itemName } = item;
  const promises = item?.images?.map((image, key) => (async () => {
    const { id, fileName: imageName } = image;
    const imageKeySuffix = key === 0 ? '' : `_${key + 1}`;

    const fileName = `${prepareFileName(itemId, itemName)}${imageKeySuffix}.${imageName.split(/[.;+_]/).pop()}`;
    const stream = await fetchImage(id, imageName);

    // TODO: find async alternative for createWriteStream
    stream.pipe(fs.createWriteStream(`${RESULT_PATH}/${fileName}`));
  })());

  return Promise.all(promises);
};
