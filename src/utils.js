import { museums } from '../static/museums';
import { SEARCH_STRING, IS_NAME_FIRST } from '../env';

export const generateUrlForItemFetch = (id) => `https://goskatalog.ru/muzfo-rest/rest/exhibits/${id}`;
export const generateUrlForItemsFetch = (from, limit) => `https://goskatalog.ru/muzfo-rest/rest/exhibits/ext?&statusIds=6&publicationLimit=false&calcCountsType=0&dirFields=desc&imageExists=true&limit=${limit}&offset=${from}&q=${SEARCH_STRING}&sortFields=id`;
export const generateUrlForQuantityFetch = () => `https://goskatalog.ru/muzfo-rest/rest/exhibits/ext?&statusIds=6&publicationLimit=false&cacheEnabled=true&calcCountsType=1&imageExists=true&limit=0&offset=0&q=${SEARCH_STRING}`;
export const generateUrlForImageFetch = (imageId, fileName) => `https://goskatalog.ru/muzfo-imaginator/rest/images/original/${imageId}?originalName=${fileName}`;
export const collectRejectedIdsFromResult = (result) => result.filter(({ status }) => status === 'rejected').map(({ reason }) => reason);
export const transformItemNameToFileNameFormat = (itemName) => itemName.replace(/[/\\?%*:|"<>]/g, '-').substring(0, 50).replace(/\.+$/, '');

export const prepareFileName = (itemId, itemName) => {
  const fileNameDefault = `${itemId}_${transformItemNameToFileNameFormat(itemName)}`;
  const fileNameWithNameFirst = `${transformItemNameToFileNameFormat(itemName)}_${itemId}`;
  const fileName = IS_NAME_FIRST ? fileNameWithNameFirst : fileNameDefault;
  return fileName;
};

export const setTimeoutPromised = (func, timeout) => new Promise((resolve) => {
  setTimeout(() => {
    resolve(func());
  }, timeout);
});

export const prepareItemDescriptionText = (item) => {
  const {
    id,
    name,
    productionPlace,
    description,
    regNumber,
    invNumber,
    gikNumber,
    museumId,
    dimStr,
    periodStr,
    technologies,
    images,
  } = item;
  const result = {
    id,
    артефакт: name,
    'період створення': periodStr,
    'місце створення': productionPlace,
    розмір: dimStr,
    'номер в госкаталозі': regNumber,
    'інвентарний номер': invNumber,
    'Номер по КП (ГИК)': gikNumber,
    опис: description,
    'кількість зображень': images?.length,
    'музей зберігання': museums.find(({ id }) => id === museumId)?.name,
    'матеріал, техніка': technologies?.map(({ name }) => name).join(', '),
  };
  return (
    `\n ----${Object.entries(result)
      .map((x) => `\n${x.join(':\n')}`)
      .join('\n')}\n --- \n`
  );
};

export class ItemCounter {
  constructor() {
    this.items = 1;
  }

  get counter() {
    return this.items;
  }

  addItem() {
    this.items += 1;
  }
}
